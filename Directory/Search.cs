﻿#region System Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class Search
    {
        /// <summary>
        /// This is the code for searching.
        /// </summary>
        public void SearchMethod(DataGridView dgvPerson, ComboBox cmbSearch, TextBox txtSearch)
        {
            try
            {
                // These strings are used for searching.
                // 'search' saves the data that has been selected in cmb.Search.
                // 'searchValue' saves the data that has been entered into the search text box.
                string search = cmbSearch.SelectedItem.ToString();
                string searchValue = txtSearch.Text;
                search = cmbSearch.Text;
                // Selects the row that has been searched for.
                dgvPerson.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

                // A switch statement is used to gather the information for each selection made by the combo box.
                switch (search)
                {
                    // If the user selects any of the choices available it will go to it.
                    case "Name":
                        // Foreach is used because I have used an array in the DataGridView.
                        // This checks the row in it.
                        foreach (DataGridViewRow row in dgvPerson.Rows)
                        {
                            // This checks the first row because 'Name' is the first row of the DataGridView.
                            if (row.Cells[0].Value.ToString().Equals(searchValue))
                            {
                                // If it has found the information it will select the row (because of the DataGridViewSelectionMod.FullRowSelect).
                                row.Selected = true;
                                break;
                            }
                        }
                        break;

                    // This is the same as "Name", however it checks in the Telephone Number row.
                    case "Telephone Number":
                        foreach (DataGridViewRow row in dgvPerson.Rows)
                        {
                            if (row.Cells[1].Value.ToString().Equals(searchValue))
                            {
                                row.Selected = true;
                                break;
                            }
                        }
                        break;

                    // This is the same as "Name", however it checks in the Email Address row.
                    case "Email Address":
                        foreach (DataGridViewRow row in dgvPerson.Rows)
                        {
                            if (row.Cells[2].Value.ToString().Equals(searchValue))
                            {
                                row.Selected = true;
                                break;
                            }
                        }
                        break;

                    // This is the same as "Name", however it checks in the Postal Address row.
                    case "Postal Address":
                        foreach (DataGridViewRow row in dgvPerson.Rows)
                        {
                            if (row.Cells[3].Value.ToString().Equals(searchValue))
                            {
                                row.Selected = true;
                                break;
                            }
                        }
                        break;

                    // This is the same as "Name", however it checks in the Birth Date row.
                    case "Birth Date":
                        foreach (DataGridViewRow row in dgvPerson.Rows)
                        {
                            if (row.Cells[4].Value.ToString().Equals(searchValue))
                            {
                                row.Selected = true;
                                break;
                            }
                        }
                        break;

                    //default:
                    //break;
                }
            }
            // If the data entered is Null/Empty it will display this error.
            catch (NullReferenceException)
            {
                MessageBox.Show("Error: Invalid Search", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If the user enters incorrect syntax, this is the message that will get displayed.
            catch(SyntaxErrorException)
            {
                MessageBox.Show("Error: Wrong Syntax", "Syntax Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If nothing is returned, this message box will be displayed.
            catch
            {
                MessageBox.Show("Error: Nothing Returned", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
