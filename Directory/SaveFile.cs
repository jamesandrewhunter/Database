﻿#region System Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class SaveFile
    {
        /// <summary>
        /// Used to save the data in the DataGridView into a text file.
        /// </summary>
        public void SaveFileMethod(DataGridView dgvPerson)
        {
            // Creates a save dialog that will be displayed once the user clicks the save button.
            // This will be used so the user can save it to a location of their choice.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            // Creates an integer called value that has been set to 0.
            int value = 0;

            // If the user selects cancel the save dialog will close otherwise it will continue onto the next code.
            if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                // This crates a stream writer that uses the save dialog which will save to the users chosen location.
                StreamWriter saveFile = new StreamWriter(saveFileDialog.FileName);
                // Creates a string that is empty.
                string stringLine = String.Empty;

                try
                {
                    //stringLine = "";
                    // If the row is empty it will skip this row loop otherwise it will be initiated.
                    // This is to gather the rows being used in the DataGridView.
                    for (int row = value; row <= dgvPerson.Rows.Count - 1; row++)
                    {
                        // This checks to see if the cell is empty or not in the DataGridView.
                        for (int cell = value; cell <= dgvPerson.Columns.Count - 1; cell++)
                        {
                            // Gets the value of the Rows an Cells via the for loops.
                            stringLine = stringLine + dgvPerson.Rows[row].Cells[cell].Value;

                            if (cell != dgvPerson.Columns.Count - 1)
                            {
                                // Adds a colon in-between each data e.g.: Bob;01012345678;
                                stringLine = stringLine + ";";
                            }
                        }
                        saveFile.WriteLine(stringLine);
                        // Converts the stringLine to empty again.
                        stringLine = String.Empty;
                    }
                    //
                    saveFile.Close();
                    MessageBox.Show("Successfully Saved File", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                // Throws the Directory Not Found Exception is the directory being used cannot be accessed.
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show("Error: Directory Not Found \nAre you use it is the right directory you are trying to save to?",
                                    "Directory Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //saveFile.Close();
                }
                // If the path is too long this exception will be thrown.
                catch (PathTooLongException)
                {
                    MessageBox.Show("Error: Path is too long to save to. Try a shorter path.",
                                    "Path Length Too Long", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //saveFile.Close();
                }
                // If the text file fails to save this error message will appear.
                catch (System.Exception)
                {
                    MessageBox.Show("Error: Save Error", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //saveFile.Close();
                }
            }
        }
    }
}
