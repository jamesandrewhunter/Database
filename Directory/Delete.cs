﻿#region System Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class Delete
    {
        /// <summary>
        /// Deletes data in the selected row.
        /// </summary>
        public void deleteMethod(DataGridView dgvPerson)
        {
            // Disables the user to add rows if the want to delete data.
            dgvPerson.AllowUserToAddRows = false;
            try
            {
                // Selects the row in the DataGridView and allows the user to delete them.
                foreach (DataGridViewRow row in dgvPerson.SelectedRows)
                {
                    // IsNewRow checks to see if the row is going to be used for new information added
                    // to the DataGridView if it is it will ignore this code otherwise it will remove the row.
                    if (!row.IsNewRow) 
                    {
                        dgvPerson.Rows.Remove(row);
                    }
                    else
                    {
                        MessageBox.Show("You are trying to delete a New Row.", "New Row Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                // Tells the user that they have successfully delete the row.
                MessageBox.Show("Deleted Data Successfully", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Automatically re-sizes the columns so the user doesn't have to do it themselves.
                dgvPerson.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            // This exception is thrown if there is no information in the row and the user it trying to delete it.
            catch (NullReferenceException)
            {
                //Console.Write(ex);
                MessageBox.Show("Error: Trying to delete an empty row.", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // If the user tries to access a row that is delete this error will be thrown.
            catch (DeletedRowInaccessibleException)
            {
                    MessageBox.Show("Error: Delete Row Inaccessible", "Deleted Row Inaccessible", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
