﻿#region System Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class Edit
    {
        /// <summary>
        /// Where all the editing happens.
        /// </summary>
        public void EditMethod(DataGridView dgvPerson)
        {
            // This creates a message box which will appear if the user presses on the edit button.
            // The first string informs the user that they have enabled edit mode.
            // The second string returns the title of the message box which tells the user what type of message it is.
            // MessageBoxButtons.Ok creates an 'OK' button on the message box so you user can easily close the message box once they have finished reading it.
            // MessageBoxIcon.Information tells the user that the information displayed on the message box is to do with information.
            MessageBox.Show("Successfully Entered Edit Mode.\n
                             You can now edit the data in the fields.", "Edit Mode", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // This turns ReadOnly off so the user can edit the values within the fields.
            dgvPerson.ReadOnly = false;
            // This automatically resizes the columns of the DataGridView to match with the current data stored in there
            // this allows the user you clearly see what is the the DataGridView without having to change the size
            // of the columns themselves.
            // AllCells automatically changes the size of the cells so the user doesn't have to do it.
            dgvPerson.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }
    }
}
