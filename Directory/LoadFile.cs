﻿#region System Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class LoadFile
    {
        /// <summary>
        /// Class file used to load a text file that the user wants.
        /// </summary>

        // When the user clicks the Load File it will display a 'Open File' window.
        OpenFileDialog openFileDialog = new OpenFileDialog();
        public void LoadFileMethod(DataGridView dgvPerson)
        {
            // Creates a string called stringLine and defines it as Empty using String.
            string stringLine = String.Empty;
            // Creates an integer called value. (Which will be used for a for loop later on
            int value = 0;
            //stringLine = "";

            // If the user selects cancel the window will close and the user will not
            // be able to load a file.
            if (openFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                //openFileDialog.InitialDirectory = @"C:\DirectoryInformation.txt";
                try
                {
                    // This reads the lines in the text file that the user has selected.
                    StreamReader loadFile = new StreamReader(openFileDialog.FileName);
                    // This disables the user to add rows (whilst it is/has loaded the file).
                    dgvPerson.AllowUserToAddRows = false;

                    // This clears the DataGridView so it can load all the data from the file.
                    dgvPerson.Rows.Clear();

                    // Converts the text inside the file into a string.
                    stringLine = loadFile.ReadLine();
                    // This splits the contents of the file into each individual cell via the use of colons.
                    string[] loadToCell = stringLine.Split(';');

                    // If the text document is empty or has no more contents it will skip this part.
                    while (stringLine != null)
                    {
                        // Adds the contents of the text boxes into rows in the DataGridView.
                        dgvPerson.Rows.Add();

                        // this repeats until there is no more information left in the text file that was loaded.
                        for (int cellCount = value; cellCount <= loadToCell.Count() - 1; cellCount++)
                        {
                            // This gets the information in between each colon and sends it to each cell.
                            loadToCell = stringLine.Split(';');
                            dgvPerson.Rows[dgvPerson.Rows.Count - 1].Cells[cellCount].Value = loadToCell[cellCount].ToString();
                        }
                        // Gets the text in the file and adds it to stringLine.
                        stringLine = loadFile.ReadLine();
                    }
                    // This closes the windows dialog.
                    loadFile.Close();
                    // Prints to the user that it has successfully loaded the file into the DataGridView.
                    MessageBox.Show("Successfully Loaded File", "Load Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // This automatically re-sizes the columns so the user doesn't have to themselves.
                    dgvPerson.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }

                // If the path is too long this error will be thrown.
                catch (PathTooLongException)
                {
                    MessageBox.Show("Error: The Path to the file is too long", "Path Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch(RowNotInTableException)
                {
                    MessageBox.Show("Error: Invalid Row", "Invalid Row", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (System.Exception)
                {
                    MessageBox.Show("Error: There seems to be a load error", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
