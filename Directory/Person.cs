﻿#region System Namespace
using System;
using System.Net.Mail;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
#endregion

namespace Directory
{
    class Person
    {
        /// <summary>
        /// Used to send the data to the DataGridView from the text boxes.
        /// </summary>
        public void PersonMethod(DataGridView dgvPerson, TextBox txtName, MaskedTextBox txtTelephoneNumber,
                                 TextBox txtPostalAddress, TextBox txtEmailAddress, MaskedTextBox txtBirthDate)
        {
            // Creates multiple strings that will be used to store the data that the user inputs in the text boxes
            // that corresponds to there name, e.g.: string Name will be used in txtName.
            string Name;
            string TelephoneNumber;
            string PostalAddress;
            string EmailAddress;
            string BirthDate;

            // Checks to see if the text boxes are empty, if they are empty it will return the MessageBox error
            // otherwise it will run the try-catch statement below.
            if (txtName.Text != "" && txtTelephoneNumber.Text != "" && txtPostalAddress.Text != ""
                && txtEmailAddress.Text != "" && txtBirthDate.Text != "")
            {
                try
                {
                    // Converts all the string declared earlier on and sets them to the corresponding text box.
                    Name = txtName.Text;
                    TelephoneNumber = txtTelephoneNumber.Text;
                    PostalAddress = txtPostalAddress.Text;
                    EmailAddress = txtEmailAddress.Text;
                    BirthDate = txtBirthDate.Text;

                    // Turns the string into an array which will be displayed in the DataGridView so the user
                    // will be able to see the what they have added into them in a graphical interface.
                    string[] row = { Name, TelephoneNumber, PostalAddress, EmailAddress, BirthDate };
                    // Adds the contents of the text boxes into rows in the DataGridView.
                    dgvPerson.Rows.Add(row);
                    // This command will auto-size the columns used in the DataGridView to match what is being
                    // displayed so you can clearly see what is in the cells rather than having to manually scroll
                    // through to see the data in it.
                    dgvPerson.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
                // If the data in the textbooks are empty this will run and will display a useful message to the user.
                catch (NullReferenceException)
                {
                    //MessageBox.Show(ex);
                    MessageBox.Show("Error: Empty Textbox", "Information Added Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (DeletedRowInaccessibleException)
                {
                    MessageBox.Show("Error: Delete Row Inaccessible", "Deleted Row Inaccessible", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch(DuplicateNameException)
                {
                    MessageBox.Show("Error: Duplicate Name", "Duplicate Name", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                // Once the user has entered the data into the text boxes and has hit the 'Add' button this set of code
                // will clear all the textbooks automatically so the user can easily add more themselves without having
                // to clear the textbooks.
                txtName.Clear();
                txtTelephoneNumber.Clear();
                txtPostalAddress.Clear();
                txtEmailAddress.Clear();
                txtBirthDate.Clear();
            }
            else
            {
                // This is the error the user will receive if they do not enter data into all the textbooks.
                MessageBox.Show("Please enter data into all text boxes.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
